<?php

namespace App\Component;
use Doctrine\Common\Collections\ArrayCollection;
use FeedIo\Feed\Item\Author;
use FeedIo\Feed\Item\AuthorInterface;
use FeedIo\Feed\Item\Media;
use FeedIo\Feed\Item\MediaInterface;
use FeedIo\Feed\ItemInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Item
 * @package App\Component
 */
class Item extends Node  implements ItemInterface
{
    /**
     * @var ArrayCollection
     * @ORM\Column(type="object")
     */
    protected $medias;

    /**
     * @var Author
     * @ORM\Column(type="object", length=200, nullable=true)
     */
    protected $author;


    public function __construct()
    {
        $this->medias = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @param  MediaInterface $media
     * @return ItemInterface
     */
    public function addMedia(MediaInterface $media) : ItemInterface
    {
        $this->medias->add($media);

        return $this;
    }

    /**
     * @return iterable
     */
    public function getMedias() : iterable
    {
        return $this->medias;
    }

    /**
     * @return boolean
     */
    public function hasMedia() : bool
    {
        return $this->medias->count() > 0;
    }

    /**
     * @return MediaInterface
     */
    public function newMedia() : MediaInterface
    {
        return new Media();
    }

    /**
     * @return AuthorInterface
     */
    public function getAuthor() : ? AuthorInterface
    {
        return $this->author;
    }

    /**
     * @param  AuthorInterface $author
     * @return ItemInterface
     */
    public function setAuthor(AuthorInterface $author = null) : ItemInterface
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return AuthorInterface
     */
    public function newAuthor() : AuthorInterface
    {
        return new Author();
    }
}
