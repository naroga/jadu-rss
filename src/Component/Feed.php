<?php

namespace App\Component;
use App\Entity\Item;
use Doctrine\Common\Collections\ArrayCollection;
use FeedIo\Feed\ItemInterface;
use FeedIo\Feed\Node\CategoryInterface;
use FeedIo\Feed\Node\ElementInterface;
use FeedIo\Feed\NodeInterface;
use FeedIo\FeedInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\Groups;


/**
 * Class Feed
 * @package App\Component
 */
class Feed extends Node implements FeedInterface, \JsonSerializable
{

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Item", mappedBy="channel", cascade={"all"})
     * @Groups({"details"})
     */
    protected $items;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024, unique=true)
     */
    protected $url;

    /**
     * Feed constructor.
     */
    public function __construct()
    {
        $this->initElements();
        $this->items = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /**
     * @return string $url
     */
    public function getUrl() : ? string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return FeedInterface
     */
    public function setUrl(string $url = null) : FeedInterface
    {
        $this->url = $url;

        return $this;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        return $this->items->current();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->items->next();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->items->key();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     *                 Returns true on success or false on failure.
     */
    public function valid()
    {
        return $this->items->current();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->items->getIterator()->rewind();
    }

    /**
     * @param  ItemInterface $item
     * @return $this
     */
    public function add(ItemInterface $item) : FeedInterface
    {
        $this->items->add($item);

        return $this;
    }

    /**
     * @return ItemInterface
     */
    public function newItem() : ItemInterface
    {
        return new Item();
    }

    /**
     * @return array
     */
    public function jsonSerialize() : array
    {
        return $this->toArray();
    }

    /**
     * @return array
     */
    public function toArray() : array
    {
        $items = [];

        foreach ($this->items as $item) {
            $items[] = $item->toArray();
        }

        $properties = parent::toArray();
        $properties['items'] = $items;

        return $properties;
    }

    /**
     * @return int
     */
    public function count() : int
    {
        return count($this->items);
    }

}
