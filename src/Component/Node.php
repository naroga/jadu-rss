<?php

namespace App\Component;

use Doctrine\Common\Collections\ArrayCollection;
use FeedIo\Feed\Node\CategoryInterface;
use FeedIo\Feed\Node\ElementInterface;
use FeedIo\Feed\NodeInterface;
use Doctrine\ORM\Mapping as ORM;


/**
 * Class Node
 * @package App\Component
 */
class Node extends \FeedIo\Feed\Node
{

    /**
     * @var ArrayCollection
     * @ORM\Column(type="object")
     */
    protected $categories;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @var ArrayCollection
     * @ORM\Column(type="object")
     */
    protected $elements;

    /**
     * @var string
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $publicId;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    protected $description;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $lastModified;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024, unique=true)
     */
    protected $link;

    /**
     * Node constructor.
     */
    public function __construct()
    {
        $this->elements = new ArrayCollection();
        $this->categories = new ArrayCollection();
    }

    /** @inheritdoc */
    protected function initElements(): void
    {
        $this->elements = new ArrayCollection();
    }

    /** @inheritdoc */
    public function addElement(ElementInterface $element)
    {
        $this->elements->add($element);
    }

    /** @inheritdoc */
    public function addCategory(CategoryInterface $category): NodeInterface
    {
        $this->categories->add($category);
        return $this;
    }
}
