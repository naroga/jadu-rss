<?php

namespace App\Repository;

use App\Entity\Channel;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Naroga\DoctrinePaginatorBundle\Page;
use Naroga\DoctrinePaginatorBundle\Paginator;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * Class ChannelRepository
 * @package App\Repository
 */
class ChannelRepository extends ServiceEntityRepository
{
    /**
     * ChannelRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Channel::class);
    }

    /**
     * Marks a channel as recently updated.
     *
     * @param Channel $channel
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function touch(Channel $channel)
    {
        $channel->setLastUpdate(new \DateTime);
        $em = $this->getEntityManager();
        $em->persist($channel);
        $em->flush($channel);
    }
}
