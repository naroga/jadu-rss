<?php

namespace App\Event;

use App\Entity\Channel;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class ChannelEvent
 * @package App\Event
 */
class ChannelEvent extends Event
{
    /** @var Channel */
    private $channel;

    private $persist = false;

    /**
     * ChannelEvent constructor.
     * @param Channel $channel
     * @param bool $persist
     */
    public function __construct(Channel $channel, bool $persist = false)
    {
        $this->channel = $channel;
        $this->persist = $persist;
    }

    /**
     * @return Channel
     */
    public function getChannel(): Channel
    {
        return $this->channel;
    }

    /**
     * @return bool
     */
    public function needsPersist(): bool
    {
        return $this->persist;
    }
}
