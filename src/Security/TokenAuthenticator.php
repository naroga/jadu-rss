<?php

namespace App\Security;


use App\Entity\AccessToken;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    public function supports(Request $request)
    {
        $hasAuthorization = $request->headers->has('Authorization');
        if (!$hasAuthorization) {
            return false;
        }

        $authorization = $request->headers->get('Authorization');
        if (strpos($authorization, 'Bearer ') !== 0) {
            return false;
        }

        return true;
    }


    /** @inheritdoc */
    public function getCredentials(Request $request)
    {
        $authorization = $request->headers->get('Authorization');
        $token = explode(' ', $authorization)[1];

        return ['token' => $token];
    }

    /** @inheritdoc */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = isset($credentials['token']) ? $credentials['token'] : null;

        if (null === $token) {
            return;
        }

        /** @var AccessToken $user */

        try {
            $user = $userProvider->loadUserByUsername($token);
        } catch (UsernameNotFoundException $e) {
            throw new BadCredentialsException;
        }

        if (!$user->isEnabled()) {
            throw new BadCredentialsException;
        }

        return $user->getUser();
    }

    /** @inheritdoc */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /** @inheritdoc */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /** @inheritdoc */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /** @inheritdoc */
    public function supportsRememberMe()
    {
        return false;
    }

    /** @inheritdoc */
    public function start(Request $request, AuthenticationException $authException = null)
    {

        $data = array(
            // you might translate this message
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }
}
