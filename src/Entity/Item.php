<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FeedIo\Feed\Item\MediaInterface;
use FeedIo\Feed\ItemInterface;
use FeedIo\Feed\Node\CategoryInterface;
use FeedIo\Feed\NodeInterface;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemRepository")
 */
class Item extends \App\Component\Item
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="item_id", type="string", length=36)
     */
    protected $id;

    /**
     * @var Channel
     * @ORM\ManyToOne(targetEntity="App\Entity\Channel", inversedBy="items")
     * @ORM\JoinColumn(name="channel_id", referencedColumnName="channel_id")
     */
    protected $channel;

    /**
     * Channel constructor.
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4();
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Channel
     */
    public function getChannel(): Channel
    {
        return $this->channel;
    }

    /**
     * @param Channel $channel
     */
    public function setChannel(Channel $channel): void
    {
        $this->channel = $channel;
    }
}
