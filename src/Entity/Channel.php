<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FeedIo\Feed\ItemInterface;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChannelRepository")
 */
class Channel extends \App\Component\Feed
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(name="channel_id", type="string", length=36)
     */
    protected $id;


    /**
     * Channel constructor.
     */
    public function __construct()
    {
        $this->id = Uuid::uuid4();
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /** @inheritdoc */
    public function newItem(): ItemInterface
    {
        $item = new Item();
        $item->setChannel($this);
        return $item;
    }
}
