<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * AccessToken
 *
 * @ORM\Entity
 * @ORM\Table(name="access_token")
 */
class AccessToken implements UserInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=36)
     * @ORM\Id
     */
    private $token;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expires_at", type="datetime")
     */
    private $expiresAt;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="user_id")
     * @Exclude
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="refresh_token", type="string", length=36, nullable=true)
     */
    private $refreshToken;

    /**
     * AccessToken constructor.
     * @param bool $refresh
     */
    public function __construct($refresh = false)
    {
        $this->token = bin2hex(random_bytes(18));
        if ($refresh) {
            $this->refreshToken = bin2hex(random_bytes(18));
        }
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return AccessToken
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * Set expiresAt
     *
     * @param \DateTime $expiresAt
     *
     * @return AccessToken
     */
    public function setExpiresAt($expiresAt)
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    /**
     * Get expiresAt
     *
     * @return \DateTime
     */
    public function getExpiresAt()
    {
        return $this->expiresAt;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }

    /**
     * @param string $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
    }

    public function isEnabled()
    {
        return $this->getExpiresAt() === null || $this->getExpiresAt() > (new \DateTime);
    }

    public function getRoles()
    {
        return $this->user->getRoles();
    }

    public function getPassword()
    {
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

}
