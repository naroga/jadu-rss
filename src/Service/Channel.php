<?php

namespace App\Service;

use App\Entity\Item;
use App\Event\ChannelEvent;
use Doctrine\ORM\EntityManager;
use FeedIo\Feed;
use FeedIo\FeedInterface;
use FeedIo\FeedIo;

/**
 * Class Channel
 * @package App\Service
 */
class Channel
{
    /** @var FeedIo */
    private $feedio;

    /** @var EntityManager */
    private $manager;

    /**
     * Channel constructor.
     * @param FeedIo $feedio
     */
    public function __construct(FeedIo $feedio, EntityManager $manager)
    {
        $this->feedio = $feedio;
        $this->manager = $manager;
    }

    /**
     * @param \App\Entity\Channel $channel
     * @param bool $persist
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateChannel(\App\Entity\Channel $channel, $persist = false)
    {

        $ttl = 15; //defaults 15 minutes

        /** @var Feed\Node\ElementInterface $element */
        foreach ($channel->getAllElements() as $element) {
            if (strtolower($element->getName()) === 'ttl') {
                $ttl = (float)$element->getValue();
                break;
            }
        }

        // If the channel has never been updated, it definitely needs to be updated.
        $needsUpdate = $channel->getLastModified() === null;

        // However, if the channel has already been updated once, we need to check if it still needs updating.
        if (!$needsUpdate) {
            $from = $channel->getLastModified()->format('U');
            $to = (new \DateTime)->format('U');

            $ageInMinutes = round(($to - $from) / 60);
            $needsUpdate = $ageInMinutes >= $ttl;
        }

        // Only updates the feed if time elapsed since last update > $ttl.
        if ($needsUpdate) {
            $this->feedio->read($channel->getUrl(), $channel, $channel->getLastModified());

            // By default, Feedio writes the datetime of the last post, instead
            // of the last check. This can cause some unnecessary checks.
            $channel->setLastModified(new \DateTime);

            if ($persist) {
                $this->manager->persist($channel);
                $this->manager->flush($channel);
            }
        }
    }

    /**
     * @param ChannelEvent $event
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateChannelOnEvent(ChannelEvent $event)
    {
        $this->updateChannel($event->getChannel(), $event->needsPersist());
    }
}
