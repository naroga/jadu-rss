<?php

namespace App\Controller;

use App\Entity\Channel;
use App\Entity\User;
use App\Event\ChannelEvent;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Naroga\DoctrinePaginatorBundle\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ChannelController extends Controller
{
    /** @var SerializerInterface */
    private $serializer;

    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * ChannelController constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer, EventDispatcherInterface $eventDispatcher)
    {
        $this->serializer = $serializer;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Creates and subscribes logged user to a new channel.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function createAndSubscribe(Request $request): JsonResponse
    {
        try {
            $url = $this->serializer->deserialize($request->getContent(), 'array', 'json')['url'];
        } catch (RuntimeException $e) {
            return new JsonResponse(['error' => 'You must provide a URL on a JSON payload.'], 400);
        }

        $em = $this
            ->getDoctrine()
            ->getManager();

        $channel = $em
            ->getRepository(Channel::class)
            ->findOneByUrl($url);

        if (!$channel) {
            $channel = new Channel;
            $channel->setUrl($url);

            // This updates the channel's data (description, title, etc).
            $this->eventDispatcher->dispatch('channel.created', new ChannelEvent($channel));
        }

        return $this->subscribe($channel);
    }

    /**
     * Retrieves a page with the current user's managed feeds.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function listSubscriptions(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Serializer $serializer */
        $serializer = $this->serializer;

        // Sets SerializationContext to Default, so items will be hidden.
        $subscriptions = $serializer->toArray(
            $user->getChannels(),
            SerializationContext::create()->setGroups(['Default'])
        );

        return new JsonResponse($subscriptions);
    }

    /**
     * Shows a channel's detailed information.
     *
     * @param Channel $channel
     * @return JsonResponse
     */
    public function showChannel(Channel $channel)
    {
        /** @var User $user */
        $user = $this->getUser();
        /** @var Serializer $serializer */
        $serializer = $this->serializer;

        $this->eventDispatcher->dispatch('channel.read', new ChannelEvent($channel, true));

        // Sets SerializationContext to Default, so items will be hidden.
        $channelData = $serializer->toArray(
            $channel,
            SerializationContext::create()->setGroups(['Default', 'details'])
        );

        // This allows us to show a [SUBSCRIBE] or [UNSUBSCRIBE] button.
        $channelData['subscribed'] = $user->getChannels()->contains($channel);

        return new JsonResponse($channelData);
    }

    /**
     * Unsubscribes current user from channel.
     *
     * @param Channel $channel
     * @return JsonResponse
     */
    public function unsubscribe(Channel $channel)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user->getChannels()->contains($channel)) {
            return new JsonResponse(['error' => 'User is not subscribed to that channel.'], 404);
        }

        $user->removeChannel($channel);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new JsonResponse(['message' => 'Unsubscribed successfully.']);
    }

    /**
     * Subscribes a user to a channel.
     *
     * @param Channel $channel
     * @return JsonResponse
     */
    public function subscribe(Channel $channel)
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user->getChannels()->contains($channel)) {
            return new JsonResponse(['message' => 'User was already subscribed to that channel'], 202);
        }

        $user->addChannel($channel);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return new JsonResponse(['message' => 'User subscribed successfully.']);
    }
}
