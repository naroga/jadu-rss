<?php

namespace App\Controller;

use App\Entity\AccessToken;
use App\Entity\User;
use App\Form\RegistrationType;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\Serializer;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * Class SecurityController
 * @package App\Controller
 */
class SecurityController extends Controller
{
    /** @var SerializerInterface */
    protected $serializer;

    /**
     * SecurityController constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function login(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $data = json_decode($request->getContent(), true);

        $rememberMe = isset($data['rememberMe']) && $data['rememberMe'] === true;

        $token = new AccessToken(!$rememberMe);

        if (!$rememberMe) {
            // 1 hour as default expiration.
            $token->setExpiresAt((new \DateTime())->add(new \DateInterval('PT1H')));
            $token->setRefreshToken(bin2hex(random_bytes(18)));
        } else {
            $token->setExpiresAt((new \DateTime())->add(date_interval_create_from_date_string('1 year')));
        }

        $token->setUser($user);

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $em->persist($token);
        $em->flush($token);

        return new JsonResponse(
            [
                'token' => $this->serializer->toArray($token),
                'user' => [
                    'name' => $user->getName()
                ]
            ]
        );
    }

    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return JsonResponse
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $data = json_decode($request->getContent(), true);
        $form = $this->createForm(RegistrationType::class, $user);

        $form->submit($data);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($user);

                $token = new AccessToken(false);
                $token->setExpiresAt((new \DateTime())->add(date_interval_create_from_date_string('1 year')));
                $token->setUser($user);

                $em->persist($token);
                $em->flush();
            } catch (UniqueConstraintViolationException $e) {
                return new JsonResponse(['error' => 'User is already registered'], 400);
            }

            return new JsonResponse(
                [
                    'token' => $this->serializer->toArray($token),
                    'user' => [
                        'name' => $user->getName()
                    ]
                ]
            );
        }

        return new JsonResponse(['errors' => $form->getErrors(true)]);
    }
}
