# Jadu Rss Reader

This is a test project, part of the Jadu screening process.

To get it working, you'll need `docker` and `docker-compose`.

###1. Starting the containers

In order to start the containers use `docker-compose`:

```$ docker-compose up -d```

This will download and setup the environment (PHP and its extensions, nginx, mysql).

You can now install the libraries:

```$ composer install```

###2. First run

Before running the application for the first time, you will need to create the database
in your mysql docker container. I made a shortcut available that you can use,
called `console`:

```$ ./console doctrine:schema:update --force```

If you are not running the application inside the provided docker containers,
you will need to change the `.env` file to reflect the location of your database.

No fixtures needed.

###3. Using the API

This project is a RESTful API for the RSS reader. It has two unprotected routes,
`/login` and `/register`, used to log in users and create new ones.

`/login` usage is below:

```
$ curl -X POST http://localhost:8008/login -d '{"username": "<your email>", "password": "<your password>", "rememberMe": false}'
{
	"token": {
		"token": "0abb4963d76f49aea003de932dd385a17280",
		"expires_at": "2018-01-28T19:35:56+00:00",
		"refresh_token": "720d693e5175372a9f24823ce31c1df060f3"
	},
	"user": {
		"name": "<user name>"
	}
}
```

The returned token will be used on the authenticated part of the application, discussed below.

Changing the `rememberMe` flag from `false` to `true` will return a long-lived (1 year) access token, but no refresh token. 

`/register` usage is below:

```
$ curl -X POST http://localhost:8008/register -d '{"name": "<your name>", "email": "<your email, will be used as username>", "plainPassword": "<your password>"}'
{
	"token": {
		"token": "0abb4963d76f49aea003de932dd385a17280",
		"expires_at": "2018-01-28T19:35:56+00:00",
		"refresh_token": "720d693e5175372a9f24823ce31c1df060f3"
	},
	"user": {
		"name": "<your name>"
	}
}
```

The return is exactly the same as `/login`, as the registration automatically
creates a valid Access Token for the newly registered user.

The first user login is always short-lived, so there is way to generate
a long-lived access token with the registration route.

Below we will discuss the authenticated part of the API. For these,
there will always need to be an `Authentication: Bearer <token>` header along with the requests.

Routes:

* `GET /channels`: lists all logged user's subscribed channels.
* `GET /channel/{id}`: lists the channel information, by its id.
* `POST /channel/{id}`: subscribes the logged user to an existing channel.
* `POST /channel`: requires a payload in the format: `{"url": "<feed url>"}`. If a channel with this URL is already registered, 
it subscribes the user to it. If not, creates the channel and subscribes the user.
* `DELETE /channel/{id}`: Unsubscribes a user to a channel.  

In order to use this project with the frontend application, you will need to set it up
separately.

### More documentation:

You can find the breakdown of how time was spent in this backend project on
[time.md](time.md).

You can find more information on the rationale for why things were built that
specific way on [rationale.md](rationale.md).
