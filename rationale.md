### Rationale for project choices

I decided to go with Symfony Flex and Doctrine2.

For RSS parsing, I went with Feedio, a Symfony bundle (flex-compatible)
for fetching the XML-based rss information and hydrating objects from there. 
Because of this choice, Doctrine2 became the obvious ORM to use, as I could just extend the
Feedio objects to add persistence mapping, and easily save them to the database.

I'm using symfony/security to create the application firewalls, and 
the authentication is a custom made Authentication Guard. I decided not to
use FOSUserBundle for the public firewall (login/register), for the reasons that follow:

* no stable version compatible with symfony flex;
* for Symfony3, most dependencies are already met with symfony-standard. With flex, however,
installing FOSUserBundle would add a ton of unnecessary bundles (forms, validation, extra, swiftmailer, among others).
* most of the features from FOSUserBundle would go unused for this small app.
* creating a custom authentication guard with Symfony is incredibly easy now.

I'm not using oauth2, because it'd be a tremendous overkill to configure a server.
We don't need any of the additional grant_types, nor scopes. Just token authentication.

Passwords are bcrypt-hashed.

I also decided to separate the backend application from the frontend application,
for the reasons that follow:

* they use separate tech stacks, so different teams can work in their own repositories without having
to deal with another team's code.
* they can and should be deployed separately. Backend needs a webserver (nginx/apache) and
thus needs an actual server (like amazon's EC2/elastic beanstalk), but the front can be served
directly from a serverless file-server, like amazon s3 + cloudfront.
* it keeps concerns separate.

Because the front and back are separate (and will run on separate domains), I'm using nelmio/cors-bundle to
allow for CORS.

Channel information is shared by all subscribed users, so if one user reads a channel feed and this
causes an update on the channel info, other users that read the same channel will read the
updated information. This prevents repeated updates on different users reading from the same channel
and preservers our precious resources.

Updates on a channel are caused by a user reading from it, or subscribing to it.

Users can freely create channels and subscribe to it, subscribe to existing channels
or unsubscribe to existing channels. They cannot, however, delete channels as these might
be in use by other users. In other words, they can manage their subscriptions.

