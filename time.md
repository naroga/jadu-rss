This is a breakdown of how time was spent in this project.

* skeleton/docker configuration: 1h
* authorization/authentication (both user-based and access-token-based): 2h
* persistent layer compatible with feedio entities: 3h
* API routes: 1.5h

TOTAL: 7.5h


